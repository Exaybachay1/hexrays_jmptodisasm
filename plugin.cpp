#include <ida.hpp>
#include <idp.hpp>
#include <dbg.hpp>
#include <loader.hpp>
#include <kernwin.hpp>

#include <hexrays.hpp>

static bool inited = false;
#define ACTION_NAME "jmptoasm:dojmp"

hexdsp_t *hexdsp = NULL;


//-------------------------------------------------------------------------
struct jmp_to_asm_ah_t: public action_handler_t
{
	virtual int idaapi activate(action_activation_ctx_t *ctx)
	{
		vdui_t &vu = *get_widget_vdui(ctx->widget);

		vu.ctree_to_disasm();

		vu.get_current_item(USE_KEYBOARD);
		citem_t *highlight = vu.item.is_citem() ? vu.item.e : NULL;

		return 1;
	}

	virtual action_state_t idaapi update(action_update_ctx_t *ctx)
	{
		return ctx->widget_type == BWN_PSEUDOCODE ? AST_ENABLE : AST_DISABLE;
	}
};
static jmp_to_asm_ah_t jmp_to_asm_ah;

static const action_desc_t jmp_to_asm = ACTION_DESC_LITERAL(
	ACTION_NAME,
	"Jump to assembly",
	&jmp_to_asm_ah,
	"J",
	NULL,
	-1
);


//--------------------------------------------------------------------------
// This callback handles various hexrays events.
static int idaapi callback(void *, hexrays_event_t event, va_list va)
{
	switch (event)
	{
	case hxe_populating_popup:
	{ // If the current item is an if-statement, then add the menu item
		TWidget *widget = va_arg(va, TWidget *);
		TPopupMenu *popup = va_arg(va, TPopupMenu *);
		vdui_t &vu = *va_arg(va, vdui_t *);
		attach_action_to_popup(widget, popup, ACTION_NAME);
	}
	break;

	default:
		break;
	}
	return 0;
}

//--------------------------------------------------------------------------
bool idaapi run(size_t)
{
	info("Jump to assembly\n"
		"This is the jump to assembly plugin.\n"
		"Press J in the decompiler view to jump to the related asm instruction\n");
	return true;
}

//--------------------------------------------------------------------------
int idaapi init(void)
{
	//hook_to_notification_point(HT_DBG, callback);

	if (!is_idaq())
		return PLUGIN_SKIP;

	if (!init_hexrays_plugin())
		return PLUGIN_SKIP;

	install_hexrays_callback(callback, NULL);
	const char *hxver = get_hexrays_version();
	msg("Hex-rays version %s has been detected, %s ready to use\n", hxver, PLUGIN.wanted_name);

	register_action(jmp_to_asm);

	msg("\nJump to assembly by florian0 loaded.\n\n\n");
	
	inited = true;

	return PLUGIN_KEEP;
}

void idaapi term(void)
{

}


//--------------------------------------------------------------------------
//
//      PLUGIN DESCRIPTION BLOCK
//
//--------------------------------------------------------------------------
plugin_t PLUGIN =
{
	IDP_INTERFACE_VERSION,
	0,                    // plugin flags
	init,                 // initialize
	term,                 // terminate. this pointer may be NULL.
	run,                  // invoke plugin
	"Jump to asm",              // long comment about the plugin
						  // it could appear in the status line
						  // or as a hint
						  "",                   // multiline help about the plugin
						  "Jump to assembly",  // the preferred short name of the plugin
						  ""                    // the preferred hotkey to run the plugin
};
